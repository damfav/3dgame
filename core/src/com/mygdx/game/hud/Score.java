package com.mygdx.game.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.Game;
import com.mygdx.game.ScoreListener;

public class Score extends Actor{
    private BitmapFont mFont;
    
    private int mTotalEnemies;
    private int mKilledEnemies;
    private String mText;
    private ScoreListener mScoreListener;
    
    public Score(){
    	
		mFont = new BitmapFont();
    	mFont.setColor(1f,1f,1f,1f);   //Brown is an underated Colour
    	
    }


    @Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		super.draw(batch, parentAlpha);
    	mFont.draw(batch, mText, 20, Gdx.graphics.getHeight() - 30);
	}


	@Override
	public Actor hit(float x, float y, boolean touchable) {
		// TODO Auto-generated method stub
		return super.hit(x, y, touchable);
	}


	public BitmapFont getFont() {
		return mFont;
	}


	public void setFont(BitmapFont font) {
		mFont = font;
	}


	public int getTotalEnemies() {
		return mTotalEnemies;
	}


	public void setTotalEnemies(int totalEnemies) {
		mTotalEnemies = totalEnemies;
	}


	public int getKilledEnemies() {
		return mKilledEnemies;
	}


	public void setKilledEnemies(int killedEnemies) {
		mKilledEnemies = killedEnemies;
		mText = mKilledEnemies + "/" + mTotalEnemies;
		mScoreListener.onScoreChange(killedEnemies);
		if (mKilledEnemies == mTotalEnemies){
			mScoreListener.onTopScore();
		}
	}


	public void setScoreListener(ScoreListener scoreListener) {
		// TODO Auto-generated method stub
		mScoreListener = scoreListener;
	}


	public String getText() {
		return mText;
	}


	public void setText(String text) {
		mText = text;
	}
	
	
	


}

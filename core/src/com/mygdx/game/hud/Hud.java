package com.mygdx.game.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.mygdx.game.Game;

public class Hud {

	private Button mLeftBtn, mRightBtn;
	private Button mShotBtn;
	private Score mScore;
	private Stage mHud;
	
	
	public Hud(){
		int width = Gdx.graphics.getWidth();
		
		TextButtonStyle style = new TextButtonStyle();
		style.font = new BitmapFont();
		mLeftBtn = new TextButton("<-", style);
		mLeftBtn.setPosition(0, 0); //** Button location **//
		mLeftBtn.setHeight(100); //** Button Height **//
		mLeftBtn.setWidth(200); //** Button Width **//
		mLeftBtn.addListener(Game.getInstance().mInputListener);
		mLeftBtn.setName("left");
		
		mRightBtn = new TextButton("->", style);
		mRightBtn.setPosition(width / 3, 0); //** Button location **//
		mRightBtn.setHeight(100); //** Button Height **//
		mRightBtn.setWidth(200); //** Button Width **//
		mRightBtn.addListener(Game.getInstance().mInputListener);
		mRightBtn.setName("right");
		
		mShotBtn = new TextButton("S", style);
		mShotBtn.setPosition(width - 200, 0); //** Button location **//
		mShotBtn.setHeight(100); //** Button Height **//
		mShotBtn.setWidth(200); //** Button Width **//
		mShotBtn.addListener(Game.getInstance().mInputListener);
		mShotBtn.setName("shot");
		
		mScore = new Score();
		mScore.setPosition(20, 20);
		mScore.setHeight(60);
		mScore.setWidth(200);
		mScore.setScoreListener(Game.getInstance());
		
		mHud = new Stage();
		mHud.addActor(mLeftBtn);
		mHud.addActor(mRightBtn);
		mHud.addActor(mShotBtn);
		mHud.addActor(mScore);
	}


	public Button getLeftBtn() {
		return mLeftBtn;
	}


	public void setLeftBtn(Button leftBtn) {
		mLeftBtn = leftBtn;
	}


	public Button getRightBtn() {
		return mRightBtn;
	}


	public void setRightBtn(Button rightBtn) {
		mRightBtn = rightBtn;
	}


	public Button getShotBtn() {
		return mShotBtn;
	}


	public void setShotBtn(Button shotBtn) {
		mShotBtn = shotBtn;
	}


	public Stage getHud() {
		return mHud;
	}


	public void setHud(Stage hud) {
		mHud = hud;
	}


	public Score getScore() {
		return mScore;
	}


	public void setScore(Score score) {
		mScore = score;
	}
	
	
	
}

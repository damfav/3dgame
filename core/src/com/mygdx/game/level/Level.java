package com.mygdx.game.level;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Game;
import com.mygdx.game.entities.Enemy;
import com.mygdx.game.entities.EnemyType1;

public class Level {

	private Model mModel;
	private ModelInstance mInstance;
	private String mLevelFile;
	
	public Level(String levelFile){
		mLevelFile = levelFile;
		mModel = Game.getInstance().assets.get(levelFile, Model.class);
		mInstance = new ModelInstance(mModel);
		mInstance.transform.rotate(0, 1,0,180);
	}
	
	public List<Enemy> getEnemies(){
		List<Enemy> enemies = new ArrayList<Enemy>();
		enemies.add(new EnemyType1(new Vector3(0,0, 100)));
		enemies.add(new EnemyType1(new Vector3(0,0, 70)));
		enemies.add(new EnemyType1(new Vector3(0,0, -100)));
		enemies.add(new EnemyType1(new Vector3(0,0, -110)));
		
		return enemies;
	}

	public Model getModel() {
		return mModel;
	}

	public void setModel(Model model) {
		mModel = model;
	}

	public ModelInstance getInstance() {
		return mInstance;
	}

	public void setInstance(ModelInstance instance) {
		mInstance = instance;
	}

	public String getLevelFile() {
		return mLevelFile;
	}

	public void setLevelFile(String levelFile) {
		mLevelFile = levelFile;
	}
	
	
	
	
}

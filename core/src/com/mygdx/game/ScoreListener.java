package com.mygdx.game;

public interface ScoreListener {

	void onScoreChange(int score);
	void onTopScore();
	
}

package com.mygdx.game;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.mygdx.game.entities.Enemy;
import com.mygdx.game.entities.Entity;
import com.mygdx.game.entities.Player;
import com.mygdx.game.hud.Hud;
import com.mygdx.game.level.Level;

public class Game implements ApplicationListener, ScoreListener{

	private List<Entity> mEntities;

	private Player mPlayer;
	
	private Environment lights;
	
	private PerspectiveCamera cam;
	
	private ModelBatch modelBatch;
	private SpriteBatch spriteBatch;
	
	public AssetManager assets;
	
	public InputListener mInputListener;
	
	private Hud hud;
	
	private Level mLevel;
	
	private static Game mInstance;
	

	public static Game getInstance(){
		if (mInstance == null){
			mInstance = new Game();
		}
		
		return mInstance;
	}
	
	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_ERROR);

		mInstance = this;
		mEntities = new ArrayList<Entity>();
		
		lights = new Environment();
		lights.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		lights.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		
		assets = new AssetManager();
		assets.load("player.g3db", Model.class);
		assets.load("level.g3db", Model.class);
		assets.load("enemy.g3db", Model.class);
		assets.load("bullet.g3db", Model.class);
		assets.finishLoading();
		
		cam = new PerspectiveCamera(80, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(20f, 4f, 0f);
		cam.lookAt(0,0,0);
		cam.near = 1f;
		cam.far = 300f;
		cam.update();
		
		modelBatch = new ModelBatch();
		spriteBatch = new SpriteBatch();
		
		mPlayer = new Player();
		mEntities.add(mPlayer);

		mLevel = new Level("level.g3db");
		
		createInputListener();
		hud = new Hud();
		Gdx.input.setInputProcessor(hud.getHud());
		
		for (Entity ent: mEntities){
			ent.getInstance().transform.scale(1.5f, 1.5f, 1.5f);
		}
		mLevel.getInstance().transform.scale(3, 3, 3);
		List<Enemy> enemies = mLevel.getEnemies();
		for (Entity en: enemies){
			addToEntities(en);
		}
		hud.getScore().setTotalEnemies(enemies.size());
		hud.getScore().setKilledEnemies(0);
	}

	
	public void update(float delta){
		for (int i = mEntities.size() - 1; i >= 0; i--){
			Entity ent = mEntities.get(i);
			ent.update(delta);
			if (ent.isDead()){
				removeEntity(ent);
			}
		}
		solveCollisions();
		cam.position.z = mPlayer.getPos().z;
		cam.update();
	}
	
	public void solveCollisions(){
		for (int i = mEntities.size() - 1; i >= 0 ; i--){
			for (int j = mEntities.size() - 1; j >= i + 1; j--){
				Entity ent1 = mEntities.get(i);
				Entity ent2 = mEntities.get(j);

				if ((ent1.getCollisionMask() & ent2.getCollisionGroup()) > 0 &&
						ent1.getBbox().intersects(ent2.getBbox())){
					Gdx.app.error("COLLISION", ent1.getClass() + " " + ent2.getClass());
					Gdx.app.error("Pos1", ent1.getPos().toString());
					Gdx.app.error("Pos2", ent2.getPos().toString());
					ent1.onCollision(ent2);
					ent2.onCollision(ent1);
		
					if (ent2.isDead()){
						removeEntity(ent2);
					}
					if (ent1.isDead()){
						removeEntity(ent1);
						break;
					}
				}
				
			}
		}
	}
	
	public void removeEntity(Entity ent) {
		// TODO Auto-generated method stub
		if (ent instanceof Enemy){
			hud.getScore().setKilledEnemies(hud.getScore().getKilledEnemies() + 1);
		}
		mEntities.remove(ent);

	}

	@Override
	public void render() {
		update(Gdx.graphics.getDeltaTime());
		
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        
        modelBatch.begin(cam);
        modelBatch.render(mLevel.getInstance(), lights);
        for (Entity ent: mEntities){
        	modelBatch.render(ent.getInstance(), lights);
        }
        modelBatch.end();
        
        spriteBatch.begin();
        hud.getHud().draw();
        spriteBatch.end();
	}
	
	@Override
	public void dispose() {
		modelBatch.dispose();
		for (Entity ent : mEntities){
			ent.getModel().dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	
	public void createInputListener(){
		mInputListener = new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            	Actor actor = event.getListenerActor();
            	String actorName = actor.getName();
            	Gdx.app.error("Input", actorName);
            	if (actorName.equals("left")){
            		mPlayer.moveLeft();
            	}else if (actorName.equals("right")){
            		mPlayer.moveRight();
            	}
            	
            	if (actorName.equals("shot")){
            		mPlayer.attack();
            	}


                return true;
            }
            
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
            	Actor actor = event.getListenerActor();
            	String actorName = actor.getName();

            	if (actorName.equals("left") || actorName.equals("right")){
            		mPlayer.stop();
            	}
            	
            }
		};
		
	}
	

	
	public void addToEntities(Entity ent){
		mEntities.add(ent);
	}

	public Player getPlayer() {
		return mPlayer;
	}

	@Override
	public void onScoreChange(int score) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTopScore() {
		// TODO Auto-generated method stub
//		Dialog dialog = new Dialog("Win", new Skin(Gdx.files.internal("uiskin.json")));
//		dialog.show(hud.getHud());
		TextButtonStyle style = new TextButtonStyle();
		style.font = new BitmapFont();
		TextButton text = new TextButton("YOU WIN", style);
		text.setWidth(100);
		text.setHeight(100);
		text.align(Align.center);
		text.setPosition(Gdx.graphics.getWidth()/2 - 100, Gdx.graphics.getHeight()/2 - 100);
		hud.getHud().addActor(text);
	}


	
	
}

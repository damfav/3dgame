package com.mygdx.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.mygdx.game.Game;
import com.mygdx.game.entities.weapon.Bullet;
import com.mygdx.game.entities.weapon.DistWeapon1;
import com.mygdx.game.entities.weapon.Weapon;

public class Player extends Character{

	private Weapon mWeapon;
	private static String MODEL_FILE = "player.g3db";
	
	public Player(){
		setModel(MODEL_FILE);
		mWeapon = new DistWeapon1(this);
		mHp = 10;
		mCollisionGroup = 1;
		mCollisionMask = 2;

	}
	
	@Override
	public void onCollision(Entity ent){
		if (ent instanceof Enemy){
//			decreaseHp(1);

		}
	}
	
	public void moveLeft(){
		mSpeed.z = 10;
	}
	
	public void moveRight(){
		mSpeed.z = -10;
	}
	
	public void attack(){
		Gdx.app.error("Attack", "PlayerAttacks");
		mWeapon.attack();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		move(delta);
	}

	public void stop() {
		// TODO Auto-generated method stub
		mSpeed.z = 0;
	}
	
	
	
}

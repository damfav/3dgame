package com.mygdx.game.entities.weapon;

import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Game;
import com.mygdx.game.entities.Entity;

public class DistWeapon1 extends DistanceWeapon {

	protected Entity mOwner;
	
	public DistWeapon1(Entity owner){
		mOwner = owner;
	}
	
	@Override
	public void attack() {
		// TODO Auto-generated method stub
		Bullet b;
		if (mOwner.isFaceRight()){
			b = new Bullet(new Vector3(0,0,15), this);
		}else{
			b = new Bullet(new Vector3(0,0,-15), this);
		}
		
		b.setPosition(mOwner.getPos());
		b.setCollisionGroup(mOwner.getCollisionGroup());
		b.setCollisionMask(mOwner.getCollisionMask());
		Game.getInstance().addToEntities(b);
		
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

}

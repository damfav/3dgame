package com.mygdx.game.entities.weapon;

import com.mygdx.game.entities.Entity;

public abstract class Weapon extends Entity{

	public abstract void attack();
}

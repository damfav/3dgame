package com.mygdx.game.entities.weapon;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Game;
import com.mygdx.game.entities.Enemy;
import com.mygdx.game.entities.Entity;

public class Bullet extends Entity {

	private DistanceWeapon mShooter;
	private static String MODEL_FILE = "bullet.g3db";
	private int mFlyTime;
	private static int MAX_FLY_TIME = 180;
	
	public Bullet(Vector3 speed, DistanceWeapon shooter){
		setModel(MODEL_FILE);
		mShooter = shooter;
		mSpeed = speed;

	}
	
	public void update(float delta){
		move(delta);
		mFlyTime++;
		if (mFlyTime > MAX_FLY_TIME){
			setDead(true);
		}
	}


	@Override
	public void onCollision(Entity ent) {
		// TODO Auto-generated method stub
		if (ent instanceof Enemy){
			setDead(true);
		}
	}

	public DistanceWeapon getShooter() {
		return mShooter;
	}

	public void setShooter(DistanceWeapon shooter) {
		mShooter = shooter;
	}
	
	
	
	
}

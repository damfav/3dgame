package com.mygdx.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.mygdx.game.Game;

public abstract class Entity {

	protected Model mModel;
	protected ModelInstance mInstance;
	protected BoundingBox mBbox;
	protected boolean mFaceRight;
	protected boolean mDead;
	protected Vector3 mSpeed = new Vector3(0,0,0);
	protected int mCollisionMask;
	protected int mCollisionGroup;

	
	public void setModel(String modelFile){
		mModel = Game.getInstance().assets.get(modelFile, Model.class);
		mInstance  = new ModelInstance(mModel);
		mBbox = new BoundingBox();

		mInstance.calculateBoundingBox(mBbox);
		Gdx.app.error("BoundingBox", mBbox.getCenter()+" " + mBbox.max + " " + mBbox.min);
	}
	
	public void move(float delta){
		if (mSpeed.equals(Vector3.Zero)){
			return;
		}
		
		float tmpZ = mSpeed.z * delta;
		float tmpX = mSpeed.x * delta;
		float tmpY = mSpeed.y * delta;
		mInstance.transform.translate(tmpX, tmpY, tmpZ);
		updateBbox();
		
		mFaceRight = mSpeed.z > 0;
		
	}
	
	public void updateBbox(){
		Vector3 pos = getPos();
		mBbox.getCenter().z = pos.z;
	}
	
	public void setPosition(Vector3 pos){
		mInstance.transform.setTranslation(pos.x, pos.y, pos.z);
		updateBbox();
	}
	
	public void onCollision(Entity ent){
		
	}

	public Model getModel() {
		return mModel;
	}

	public void setModel(Model model) {
		mModel = model;
	}

	public ModelInstance getInstance() {
		return mInstance;
	}

	public void setInstance(ModelInstance instance) {
		mInstance = instance;
	}

	public BoundingBox getBbox() {
		return mBbox;
	}

	public void setBbox(BoundingBox bbox) {
		mBbox = bbox;
	}

	public boolean isFaceRight() {
		return mFaceRight;
	}

	public void setFaceRight(boolean faceRight) {
		mFaceRight = faceRight;
	}
	
	public abstract void update(float delta);
	
	public Vector3 getPos(){
		return mInstance.transform.getTranslation(new Vector3(0,0,0));
	}

	public boolean isDead() {
		return mDead;
	}

	public void setDead(boolean dead) {
		mDead = dead;
	}

	public int getCollisionMask() {
		return mCollisionMask;
	}

	public void setCollisionMask(int collisionMask) {
		mCollisionMask = collisionMask;
	}

	public int getCollisionGroup() {
		return mCollisionGroup;
	}

	public void setCollisionGroup(int collisionGroup) {
		mCollisionGroup = collisionGroup;
	}

	public Vector3 getSpeed() {
		return mSpeed;
	}

	public void setSpeed(Vector3 speed) {
		mSpeed = speed;
	}
	
	
	
	
}

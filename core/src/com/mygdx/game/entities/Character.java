package com.mygdx.game.entities;


public abstract class Character extends Entity{

	protected int mHp;
	

	public void decreaseHp(int dmg){
		mHp -= dmg;
		if (mHp <= 0){
			onDead();
		}
	}
	
	
	public void onDead(){
		setDead(true);
	}
	
}

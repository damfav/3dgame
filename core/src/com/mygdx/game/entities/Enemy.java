package com.mygdx.game.entities;

import com.mygdx.game.entities.weapon.Bullet;

public abstract class Enemy extends Character{

	public Enemy(){
		super();
		mCollisionGroup = 2;
		mCollisionMask = 1;
	}
	
	@Override
	public void onCollision(Entity ent){
		if (ent.getClass() == Bullet.class){
			decreaseHp(1);
		}
	}
	

	
}

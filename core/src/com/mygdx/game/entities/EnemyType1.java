package com.mygdx.game.entities;

import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Game;

public class EnemyType1 extends Enemy{

	private static String MODEL_FILE = "enemy.g3db";
	private static int MAX_HP = 10;
	
	public EnemyType1(Vector3 pos){
		setModel(MODEL_FILE);
		mInstance.transform.setTranslation(pos);
		mInstance.transform.scale(3, 3, 3);
		mHp = MAX_HP;
		updateBbox();
		
	}
	
	@Override
	public void update(float delta){
		Player p = Game.getInstance().getPlayer();
		Vector3 pPos = p.getPos();
		Vector3 thisPos = getPos();
		if (pPos.z > thisPos.z){
			mSpeed.z = 1f;
		}else{
			mSpeed.z = -1f;
		}
		
		move(delta);
	}


}
